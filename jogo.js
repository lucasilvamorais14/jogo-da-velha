const p1 = "url(img/mario.png)"; //Definir imagem do player 1.
const p2 = "url(img/sonic.png)"; //Definir imagem do player 2.
var vez = 1; //Definir de quem é a vez.
var plays = 0; //Definir quantas rodadas já foram jogadas.
var over = false; //Verificar o fim do jogo.
var mario = 0;
document.getElementById("pM").innerHTML = "<h1>Mario "+mario+" </h1>";
var sonic = 0;
document.getElementById("pS").innerHTML = "<h1> "+sonic+" Sonic</h1>";
const pos = [[1,2,3],[4,5,6],[7,8,9],
			 [1,4,7],[2,5,8],[3,6,9],
			 [1,5,9],[3,5,7]]; //Possibilidades.

function play(h){ 
	if (over == false){
		quadrado = document.getElementById(h); 
		if (quadrado.style.backgroundImage == "none" || quadrado.style.backgroundImage == ""){ //Verifica se o espaço está vazio.
			plays++; //Adiciona +1 nas rodadas.
			if (vez == 1){ //Verificar se é a vez do player 1.
				quadrado.style.backgroundImage = p1; //Define imagem do player 1 no espaço.
				vez = 2; //Passa a vez para o player 2.
				document.getElementById("vez").innerHTML = "Vez do Sonic"; //Edita o texto no h2 do html.
			}else{
				quadrado.style.backgroundImage = p2; //Define imagem do player 2 no espaço.
				vez = 1; //Passa a vez para o player 1.
				document.getElementById("vez").innerHTML = "Vez do Mario"; //Edita o texto no h2 do html.
			}
			verificarVencedor(); //Verificar.
		}
	}else{
		return; //Impede novas jogadas depois do fim do jogo.
	}
}

function verificarVencedor(){
	if (plays > 4){ //Verifica se a quantidade de rodadas é maior do que quatro (Só é possível ganhar no jogo da velha a partir de cinco rodadas).
		for (var i = 0; i < pos.length; i++){ //Verifica cada possibilidade.
			e1 = document.getElementById(pos[i][0]).style.backgroundImage; //Primeiro vetor
			e2 = document.getElementById(pos[i][1]).style.backgroundImage; //Segundo vetor
			e3 = document.getElementById(pos[i][2]).style.backgroundImage; //Terceiro vetor
			if (e1 == e2 && e1 == e3 && e1 !== ""){
				if (vez == 2){
					mario += 1;
					document.getElementById("pM").innerHTML = "<h1>Mario "+mario+" </h1>";
					document.getElementById("resultado").innerHTML = "Mario venceu!";
				}else{
					sonic +=1;
					document.getElementById("pS").innerHTML = "<h1> "+sonic+" Sonic</h1>";
					document.getElementById("resultado").innerHTML = "Sonic venceu!";
				}
				over = true; //Fim do jogo.
				return;
			}
			
			
		}
		if (plays > 8 && over == false){ //Verifica se deu empate.
			document.getElementById("resultado").innerHTML = "Deu velha!";
			over = true; //Fim do jogo.
		}
	}
}

function reset(){ //Reinicia o jogo.
	plays = 0;
	over = false;
	vez = 1;
	document.getElementById("vez").innerHTML = "Vez do Mario";
	document.getElementById("resultado").innerHTML = "";
	for (var i = 1; i <= 9; i++){ //Definir cada espaço como vazio.
		quadrado = document.getElementById(i);
		quadrado.style.backgroundImage = "";
	}
}

function resetPlacar(){
	sonic = 0;
	mario = 0;
	document.getElementById("pM").innerHTML = "<h1>Mario "+mario+" </h1>";
	document.getElementById("pS").innerHTML = "<h1> "+sonic+" Sonic</h1>";
}